module Hello exposing (..)

import Autocomplete
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode
import Process
import Task
import Time


main : Program Flags Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.map SetAutoState Autocomplete.subscription


type alias Model =
    { plantTypes : List PlantType
    , query : String
    , autoState : Autocomplete.State
    , howManyToShow : Int
    , selectedPlantType : Maybe PlantType
    , showMenu : Bool
    }


init : Flags -> ( Model, Cmd msg )
init flags =
    case ( Json.Decode.decodeValue plantTypesDecoder flags.plantTypes, Json.Decode.decodeValue (Json.Decode.nullable plantTypeDecoder) flags.selectedPlantType ) of
        ( Ok plantTypes, Ok selectedPlantType ) ->
            ( { initModel
                | plantTypes = plantTypes
                , selectedPlantType = selectedPlantType
              }
            , Cmd.none
            )

        ( Err err, _ ) ->
            Debug.crash err

        ( _, Err err ) ->
            Debug.crash err


plantTypesDecoder : Json.Decode.Decoder (List PlantType)
plantTypesDecoder =
    Json.Decode.list plantTypeDecoder


plantTypeDecoder : Json.Decode.Decoder PlantType
plantTypeDecoder =
    Json.Decode.map2 PlantType
        (Json.Decode.map toString (Json.Decode.field "id" Json.Decode.int))
        (Json.Decode.field "name" Json.Decode.string)


initModel : Model
initModel =
    { plantTypes = []
    , query = ""
    , autoState = Autocomplete.empty
    , howManyToShow = 5
    , selectedPlantType = Nothing
    , showMenu = False
    }


type Msg
    = SetQuery String
    | OnFocus
    | OnBlur
    | SetAutoState Autocomplete.Msg
    | SelectPlantTypeMouse String
    | SelectPlantTypeKeyboard String
    | PreviewPlantType String
    | HideMenu
    | Wrap Bool
    | Reset
    | NoOp


type alias Flags =
    { plantTypes : Json.Decode.Value
    , selectedPlantType : Json.Decode.Value
    }


type alias PlantType =
    { id : String
    , name : String
    }


acceptablePlantTypes : String -> List PlantType -> List PlantType
acceptablePlantTypes query plantTypes =
    let
        lowerQuery =
            String.toLower query
    in
        List.filter (String.contains lowerQuery << String.toLower << .name) plantTypes


updateConfig : Autocomplete.UpdateConfig Msg PlantType
updateConfig =
    Autocomplete.updateConfig
        { toId = .id
        , onKeyDown =
            \code maybeId ->
                if code == 38 || code == 40 then
                    Maybe.map PreviewPlantType maybeId
                else if code == 13 then
                    Maybe.map SelectPlantTypeKeyboard maybeId
                else
                    Nothing
        , onTooLow = Just (Wrap False)
        , onTooHigh = Just (Wrap True)
        , onMouseEnter = \_ -> Nothing
        , onMouseLeave = \_ -> Nothing
        , onMouseClick = \id -> Just (SelectPlantTypeMouse id)
        , separateSelections = False
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnFocus ->
            ( { model | showMenu = True }, Cmd.none )

        OnBlur ->
            ( model, delay (200 * Time.millisecond) HideMenu )

        HideMenu ->
            ( { model | showMenu = False }, Cmd.none )

        SetQuery newQuery ->
            let
                lowerQuery =
                    String.toLower newQuery

                newSelectedPlantType =
                    model.plantTypes
                        |> List.filter (\p -> String.toLower p.name == lowerQuery)
                        |> List.head

                showMenu =
                    let
                        searchResult =
                            (acceptablePlantTypes newQuery model.plantTypes)
                    in
                        case newSelectedPlantType of
                            Just plantType ->
                                List.length searchResult > 1

                            Nothing ->
                                searchResult
                                    |> List.isEmpty
                                    |> not
            in
                ( { model | query = newQuery, selectedPlantType = newSelectedPlantType, showMenu = showMenu }, Cmd.none )

        SetAutoState autoMsg ->
            let
                ( newState, maybeMsg ) =
                    Autocomplete.update updateConfig autoMsg model.howManyToShow model.autoState (acceptablePlantTypes model.query model.plantTypes)

                newModel =
                    { model | autoState = newState }
            in
                case maybeMsg of
                    Nothing ->
                        ( newModel, Cmd.none )

                    Just updateMsg ->
                        update updateMsg newModel

        SelectPlantTypeMouse id ->
            let
                newModel =
                    model
                        |> setSelectedPlantType id
                        |> resetMenu
            in
                ( newModel, Cmd.none )

        SelectPlantTypeKeyboard id ->
            let
                newModel =
                    model
                        |> setSelectedPlantType id
                        |> resetMenu
            in
                ( newModel, Cmd.none )

        PreviewPlantType id ->
            let
                newModel =
                    model
                        |> setSelectedPlantType id
            in
                ( newModel, Cmd.none )

        Wrap toTop ->
            case model.selectedPlantType of
                Just plantType ->
                    update Reset model

                Nothing ->
                    if toTop then
                        { model
                            | autoState = Autocomplete.resetToLastItem updateConfig (acceptablePlantTypes model.query model.plantTypes) model.howManyToShow model.autoState
                            , selectedPlantType = List.head <| List.reverse <| List.take model.howManyToShow <| (acceptablePlantTypes model.query model.plantTypes)
                        }
                            ! []
                    else
                        { model
                            | autoState = Autocomplete.resetToFirstItem updateConfig (acceptablePlantTypes model.query model.plantTypes) model.howManyToShow model.autoState
                            , selectedPlantType = List.head <| List.take model.howManyToShow <| (acceptablePlantTypes model.query model.plantTypes)
                        }
                            ! []

        Reset ->
            ( { model | autoState = Autocomplete.reset updateConfig model.autoState, selectedPlantType = Nothing }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


delay : Time.Time -> msg -> Cmd msg
delay time msg =
    Process.sleep time
        |> Task.perform (\_ -> msg)


getPlantTypeWithId : List PlantType -> String -> Maybe PlantType
getPlantTypeWithId plantTypes id =
    List.filter (\plantType -> plantType.id == id) plantTypes
        |> List.head


removeSelectedPlantType : Model -> Model
removeSelectedPlantType model =
    { model | selectedPlantType = Nothing }


setSelectedPlantType : String -> Model -> Model
setSelectedPlantType id model =
    { model | selectedPlantType = getPlantTypeWithId model.plantTypes id }


resetMenu : Model -> Model
resetMenu model =
    { model
        | autoState = Autocomplete.empty
        , showMenu = False
    }



-- setup for your autocomplete view


viewConfig : Autocomplete.ViewConfig PlantType
viewConfig =
    let
        customizedLi keySelected mouseSelected plantType =
            { attributes = [ classList [ ( "autocomplete-item", True ), ( "is-selected", keySelected || mouseSelected ) ] ]
            , children = [ Html.text plantType.name ]
            }
    in
        Autocomplete.viewConfig
            { toId = .id
            , ul = [ class "autocomplete-list" ] -- set classes for your list
            , li = customizedLi -- given selection states and a person, create some Html!
            }



-- and let's show it! (See an example for the full code snippet)


view : Model -> Html Msg
view model =
    let
        decodeKeyDown =
            (Json.Decode.map
                (\code ->
                    if code == 38 || code == 40 then
                        Ok NoOp
                    else if code == 13 then
                        Ok NoOp
                    else
                        Err "not handling that key"
                )
                keyCode
            )
                |> Json.Decode.andThen
                    fromResult

        fromResult : Result String a -> Json.Decode.Decoder a
        fromResult result =
            case result of
                Ok val ->
                    Json.Decode.succeed val

                Err reason ->
                    Json.Decode.fail reason

        query =
            case model.selectedPlantType of
                Just plantType ->
                    plantType.name

                Nothing ->
                    model.query

        menu =
            if model.showMenu then
                [ viewMenu model ]
            else
                []

        plantTypeIdInput =
            [ viewHiddenPlantTypeInput model ]
    in
        div []
            (List.concat
                [ [ input
                        [ onInput SetQuery
                        , onFocus OnFocus
                        , onBlur OnBlur
                        , onWithOptions "keydown" { preventDefault = True, stopPropagation = False } decodeKeyDown
                        , value query
                        , autocomplete False
                        ]
                        []
                  ]
                , menu
                , plantTypeIdInput
                ]
            )


viewHiddenPlantTypeInput : Model -> Html Msg
viewHiddenPlantTypeInput model =
    case model.selectedPlantType of
        Just plantType ->
            input
                [ name "sprout[plant_type_id]"
                , value plantType.id
                , attribute "type" "hidden"
                ]
                []

        Nothing ->
            input
                [ name "sprout[plant_type][name]"
                , value model.query
                , attribute "type" "hidden"
                ]
                []


viewMenu : Model -> Html Msg
viewMenu model =
    div [ class "autocomplete-menu" ]
        [ Html.map SetAutoState (Autocomplete.view viewConfig model.howManyToShow model.autoState (acceptablePlantTypes model.query model.plantTypes)) ]
