# SproutTree

To start your Phoenix server:

* Install dependencies with `mix deps.get`
* Create and migrate your database with `mix ecto.create && mix ecto.migrate`
* Seed your database with `mix run priv/repo/seeds.exs`
* Install Node.js dependencies with `cd assets && npm install`
* Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Prerequisites

* [Install Elixir](https://elixir-lang.org/install.html): `brew install elixir`
* [Install Hex](https://elixir-lang.org/install.html): `mix local.hex`
* Install nodejs: `brew install nodejs`
* [Install Postgres](https://www.postgresql.org/download/):

  ```bash
  brew install postgres
  brew services start postgresql

  # Create new database
  createdb

  # Create user 'postgres' in database
  psql -c "CREATE ROLE postgres LOGIN CREATEDB;"

  # Give user the proper rights
  psql -c "ALTER ROLE postgres LOGIN;"
  psql -c "ALTER ROLE postgres CREATEDB;"
  ```

## Glossary

* **Sprout**: A plant
* **Botanist**: A user that owns Sprouts
* **PMS**: Plant Management System
* **Sprout invitation**: An invitation to connect a Sprout with another sprout.
* **Plant type**: A type of plant

## Learn more

* Official website: http://www.phoenixframework.org/
* Guides: http://phoenixframework.org/docs/overview
* Docs: https://hexdocs.pm/phoenix
* Mailing list: http://groups.google.com/group/phoenix-talk
* Source: https://github.com/phoenixframework/phoenix
