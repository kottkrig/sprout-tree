use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :sprout_tree, SproutTreeWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :sprout_tree, SproutTree.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "sprout_tree_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :sprout_tree, SproutTree.Accounts.Auth.Guardian,
  # Name of your app/company/product
  issuer: "sprout_tree",
  # Replace this with the output of the mix command
  secret_key: "test_secret_key"
