use Mix.Config

import_config "test.exs"

# Configure your database
config :sprout_tree, SproutTree.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "sprout_tree_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox
