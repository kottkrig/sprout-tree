defmodule SproutTree.Repo.Migrations.CreateBotanists do
  use Ecto.Migration

  def change do
    create table(:botanists) do
      add(:bio, :text)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(unique_index(:botanists, [:user_id]))
  end
end
