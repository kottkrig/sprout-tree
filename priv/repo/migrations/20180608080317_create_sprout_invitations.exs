defmodule SproutTree.Repo.Migrations.CreateSproutInvitations do
  use Ecto.Migration

  def change do
    create table(:sprout_invitations) do
      add(:verification_code, :string)
      add(:new_sprout_type, :integer)
      add(:sprout_id, references(:sprouts, on_delete: :delete_all))

      timestamps()
    end

    create(unique_index(:sprout_invitations, [:verification_code]))
    create(index(:sprout_invitations, [:sprout_id]))
  end
end
