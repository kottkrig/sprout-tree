defmodule SproutTree.Repo.Migrations.CreateSproutRequests do
  use Ecto.Migration

  def change do
    create table(:sprout_requests) do
      add(:message, :text)
      add(:status, :integer, default: 0)
      add(:sender_botanist_id, references(:botanists, on_delete: :nothing))
      add(:receiver_botanist_id, references(:botanists, on_delete: :nothing))
      add(:parent_sprout_id, references(:sprouts, on_delete: :nothing))

      timestamps()
    end

    create(index(:sprout_requests, [:sender_botanist_id]))
    create(index(:sprout_requests, [:receiver_botanist_id]))
    create(index(:sprout_requests, [:parent_sprout_id]))
  end
end
