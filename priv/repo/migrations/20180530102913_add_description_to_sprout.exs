defmodule SproutTree.Repo.Migrations.AddDescriptionToSprout do
  use Ecto.Migration

  def change do
    alter table(:sprouts) do
      add(:description, :text)
    end
  end
end
