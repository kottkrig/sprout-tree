defmodule SproutTree.Repo.Migrations.AddNameToSprout do
  use Ecto.Migration

  def change do
    alter table(:sprouts) do
      add(:name, :string)
    end
  end
end
