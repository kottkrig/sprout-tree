defmodule SproutTree.Repo.Migrations.CreatePlantTypes do
  use Ecto.Migration

  def change do
    create table(:plant_types) do
      add :name, :string

      timestamps()
    end

  end
end
