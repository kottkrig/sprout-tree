defmodule SproutTree.Repo.Migrations.CreateSprouts do
  use Ecto.Migration

  def change do
    create table(:sprouts) do
      add :plant_type_id, references(:plant_types, on_delete: :nothing)
      add :owner_botanist_id, references(:botanists, on_delete: :nothing)
      add :parent_sprout_id, references(:sprouts, on_delete: :nothing)

      timestamps()
    end

    create index(:sprouts, [:plant_type_id])
    create index(:sprouts, [:owner_botanist_id])
    create index(:sprouts, [:parent_sprout_id])
  end
end
