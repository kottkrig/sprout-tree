# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SproutTree.Repo.insert!(%SproutTree.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias SproutTree.Accounts

admin_params = %{
  first_name: "Admin",
  full_name: "Admin User",
  is_admin: true,
  credential: %{email: "admin@test.com", password: "1234"}
}

Accounts.create_user(admin_params)
