defmodule SproutTreeWeb.PlantTypeView do
  use SproutTreeWeb, :view

  def render("index.json", %{plant_types: plant_types}) do
    Enum.map(plant_types, &plant_type_json/1)
  end

  def render("show.json", %{plant_type: plant_type}) do
    plant_type_json(plant_type)
  end

  def plant_type_json(plant_type) do
    %{
      id: plant_type.id,
      name: plant_type.name
    }
  end
end
