defmodule SproutTreeWeb.PMS.SproutView do
  use SproutTreeWeb, :view
  alias SproutTree.PMS
  alias SproutTree.PMS.{PlantType, Sprout}

  def sprout_title(%Sprout{name: sprout_name}) when not is_nil(sprout_name) do
    sprout_name
  end

  def sprout_title(%Sprout{
        plant_type: %PlantType{name: plant_type_name},
        parent_sprout: %Sprout{
          owner_botanist: parent_botanist
        }
      }) do
    "#{plant_type_name} från #{parent_botanist.user.first_name}"
  end

  def sprout_title(
        %Sprout{
          plant_type: %PlantType{name: plant_type_name}
        } = sprout
      ) do
    plant_type_name
  end

  def botanist_name(%PMS.Sprout{owner_botanist: botanist}) do
    botanist.user.first_name
  end

  def plant_types_to_json(plant_types) do
    SproutTreeWeb.PlantTypeView.render("index.json", plant_types: plant_types)
    |> Poison.encode!()
  end

  def plant_type_to_json(plant_type) when is_nil(plant_type) do
    nil
  end

  def plant_type_to_json(plant_type) do
    SproutTreeWeb.PlantTypeView.render("show.json", plant_type: plant_type)
    |> Poison.encode!()
  end
end
