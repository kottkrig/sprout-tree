defmodule SproutTreeWeb.PMS.SproutInvitationView do
  use SproutTreeWeb, :view

  alias SproutTree.Accounts.{User}
  alias SproutTree.PMS.{Botanist, PlantType, Sprout, SproutInvitation}

  def sprout_invitation_title(%SproutInvitation{
        new_sprout_type: :is_child,
        sprout: %Sprout{
          plant_type: %PlantType{name: plant_type_name},
          owner_botanist: %Botanist{
            user: %User{
              first_name: first_name
            }
          }
        }
      }) do
    "#{plant_type_name} från #{first_name}"
  end

  def sprout_invitation_title(%SproutInvitation{
        new_sprout_type: :is_parent,
        sprout: %Sprout{
          plant_type: %PlantType{name: plant_type_name},
          owner_botanist: %Botanist{
            user: %User{
              first_name: first_name
            }
          }
        }
      }) do
    "Har #{first_name} fått #{plant_type_name} från dig?"
  end

  def sprout_invitation_title(%SproutInvitation{
        sprout: %Sprout{
          plant_type: %PlantType{name: plant_type_name}
        }
      }) do
    plant_type_name
  end
end
