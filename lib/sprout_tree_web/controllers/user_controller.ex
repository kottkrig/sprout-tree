defmodule SproutTreeWeb.UserController do
  use SproutTreeWeb, :controller

  alias SproutTree.Accounts
  alias SproutTree.Accounts.User
  alias SproutTree.PMS

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        _botanist = PMS.ensure_botanist_exists(user)

        conn
        |> Accounts.Auth.Guardian.Plug.sign_in(user)
        |> put_flash(:info, "Välkommen!")
        |> redirect(to: pms_sprout_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
