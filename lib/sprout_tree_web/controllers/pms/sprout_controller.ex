defmodule SproutTreeWeb.PMS.SproutController do
  use SproutTreeWeb, :controller

  alias SproutTree.PMS
  alias SproutTree.PMS.Sprout

  plug(:require_existing_botanist)
  plug(:authorize_sprout when action in [:edit, :update, :delete])

  def index(conn, _params) do
    sprouts = PMS.list_sprouts_for_botanist(conn.assigns.current_botanist)
    render(conn, "index.html", sprouts: sprouts)
  end

  def new(conn, _params) do
    plant_types = PMS.list_plant_types()
    changeset = PMS.change_sprout(%Sprout{})
    render(conn, "new.html", plant_types: plant_types, changeset: changeset)
  end

  def create(conn, %{"sprout" => sprout_params}) do
    case PMS.create_sprout(conn.assigns.current_botanist, sprout_params) do
      {:ok, sprout} ->
        conn
        |> put_flash(:info, "Sprout created successfully.")
        |> redirect(to: pms_sprout_path(conn, :show, sprout))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    sprout = PMS.get_sprout!(id)
    botanists = PMS.list_botanists()
    render(conn, "show.html", sprout: sprout, botanists: botanists)
  end

  def edit(conn, _) do
    plant_types = PMS.list_plant_types()
    changeset = PMS.change_sprout(conn.assigns.sprout)
    render(conn, "edit.html", plant_types: plant_types, changeset: changeset)
  end

  def update(conn, %{"sprout" => sprout_params}) do
    case PMS.update_sprout(conn.assigns.sprout, sprout_params) do
      {:ok, sprout} ->
        conn
        |> put_flash(:info, "Sprout updated successfully.")
        |> redirect(to: pms_sprout_path(conn, :show, sprout))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    {:ok, _sprout} = PMS.delete_sprout(conn.assigns.sprout)

    conn
    |> put_flash(:info, "Sprout deleted successfully.")
    |> redirect(to: pms_sprout_path(conn, :index))
  end

  defp require_existing_botanist(conn, _) do
    botanist = PMS.ensure_botanist_exists(conn.assigns.maybe_user)
    assign(conn, :current_botanist, botanist)
  end

  defp authorize_sprout(conn, _) do
    sprout = PMS.get_sprout!(conn.params["id"])

    if conn.assigns.current_botanist.id == sprout.owner_botanist_id do
      assign(conn, :sprout, sprout)
    else
      conn
      |> put_flash(:error, "You can't modify that page")
      |> redirect(to: pms_sprout_path(conn, :index))
      |> halt()
    end
  end
end
