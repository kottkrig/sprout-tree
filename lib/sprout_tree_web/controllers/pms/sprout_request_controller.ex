defmodule SproutTreeWeb.PMS.SproutRequestController do
  use SproutTreeWeb, :controller

  alias SproutTree.PMS
  alias SproutTree.PMS.SproutRequest

  plug(:require_existing_botanist)

  def index(conn, _params) do
    sprout_requests = PMS.list_sprout_requests_for_botanist(conn.assigns.current_botanist)
    render(conn, "index.html", sprout_requests: sprout_requests)
  end

  def new(conn, %{"sprout_id" => sprout_id}) do
    sprout = PMS.get_sprout!(sprout_id)
    changeset = PMS.change_sprout_request(%SproutRequest{})
    botanists = PMS.list_botanists()

    render(conn, "new.html", changeset: changeset, botanists: botanists, sprout: sprout)
  end

  def create(conn, %{"sprout_id" => sprout_id, "sprout_request" => sprout_request_params}) do
    sprout = PMS.get_sprout!(sprout_id)

    case PMS.create_sprout_request(sprout, sprout_request_params) do
      {:ok, sprout_request} ->
        conn
        |> put_flash(:info, "Sprout request created successfully.")
        |> redirect(to: pms_sprout_path(conn, :show, sprout))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    sprout = PMS.get_sprout!(id)
    render(conn, "show.html", sprout: sprout)
  end

  def edit(conn, _) do
    plant_types = PMS.list_plant_types()
    changeset = PMS.change_sprout(conn.assigns.sprout)
    render(conn, "edit.html", plant_types: plant_types, changeset: changeset)
  end

  def update(conn, %{"id" => id, "sprout_request" => sprout_request_params}) do
    sprout_request = PMS.get_sprout_request!(id)

    case PMS.update_sprout_request(sprout_request, sprout_request_params) do
      {:ok, sprout_request} ->
        conn
        |> put_flash(:info, "Sprout updated successfully.")
        |> redirect(to: pms_sprout_request_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    {:ok, _sprout} = PMS.delete_sprout(conn.assigns.sprout)

    conn
    |> put_flash(:info, "Sprout deleted successfully.")
    |> redirect(to: pms_sprout_path(conn, :index))
  end

  defp require_existing_botanist(conn, _) do
    botanist = PMS.ensure_botanist_exists(conn.assigns.maybe_user)
    assign(conn, :current_botanist, botanist)
  end
end
