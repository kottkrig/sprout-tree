defmodule SproutTreeWeb.PMS.SproutInvitationController do
  use SproutTreeWeb, :controller

  alias SproutTree.PMS
  alias SproutTree.PMS.SproutInvitation

  def generate(conn, %{"sprout_id" => sprout_id}) do
    sprout = PMS.get_sprout!(sprout_id)

    case PMS.generate_child_sprout_invitation(sprout) do
      {:ok, sprout_invitation} ->
        conn
        |> put_flash(:info, "Sprout invitation created successfully.")
        |> redirect(
          to: pms_sprout_invitation_path(conn, :show, sprout_invitation.verification_code)
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Sprout invitation couldn't be created.")
        |> redirect(to: pms_sprout_path(conn, :show, sprout))
    end
  end

  def generate_parent(conn, %{"sprout_id" => sprout_id}) do
    sprout = PMS.get_sprout!(sprout_id)

    case PMS.generate_parent_sprout_invitation(sprout) do
      {:ok, sprout_invitation} ->
        conn
        |> put_flash(:info, "Sprout invitation created successfully.")
        |> redirect(
          to: pms_sprout_invitation_path(conn, :show, sprout_invitation.verification_code)
        )

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Sprout invitation couldn't be created.")
        |> redirect(to: pms_sprout_path(conn, :show, sprout))
    end
  end

  def accept(conn, %{"id" => id}) do
    sprout_invitation = PMS.get_sprout_invitation!(id)
    botanist = PMS.ensure_botanist_exists(conn.assigns.maybe_user)

    case PMS.accept_sprout_invitation(botanist, sprout_invitation) do
      {:ok, sprout} ->
        conn
        |> put_flash(:info, "Sprout invitation created successfully.")
        |> redirect(to: pms_sprout_path(conn, :edit, sprout))

      {:error, _} ->
        conn
        |> put_flash(:error, "Sprout invitation couldn't be created.")
        |> redirect(
          to: pms_sprout_invitation_path(conn, :show, sprout_invitation.verification_code)
        )
    end
  end

  def accept_to_existing_sprout(conn, %{"id" => id, "existing_sprout_id" => existing_sprout_id}) do
    sprout_invitation = PMS.get_sprout_invitation!(id)
    botanist = PMS.ensure_botanist_exists(conn.assigns.maybe_user)
    existing_sprout = PMS.get_sprout!(existing_sprout_id)

    case PMS.accept_sprout_invitation_to_existing_sprout(
           botanist,
           sprout_invitation,
           existing_sprout
         ) do
      {:ok, sprout} ->
        conn
        |> put_flash(:info, "Sprout invitation created successfully.")
        |> redirect(to: pms_sprout_path(conn, :edit, sprout))

      {:error, _} ->
        conn
        |> put_flash(:error, "Sprout invitation couldn't be created.")
        |> redirect(
          to: pms_sprout_invitation_path(conn, :show, sprout_invitation.verification_code)
        )
    end
  end

  def show(conn, %{"verification_code" => verification_code}) do
    botanist = PMS.ensure_botanist_exists(conn.assigns.maybe_user)
    sprout_invitation = PMS.get_sprout_invitation_with_verification_code!(verification_code)

    existing_sprouts =
      case sprout_invitation.new_sprout_type do
        :is_child ->
          botanist
          |> PMS.list_sprouts_for_botanist()
          |> Enum.filter(&is_nil(&1.parent_sprout_id))

        :is_parent ->
          PMS.list_sprouts_for_botanist(botanist)
      end

    render(
      conn,
      "show.html",
      sprout_invitation: sprout_invitation,
      existing_sprouts: existing_sprouts
    )
  end

  def delete(conn, %{"id" => id}) do
    sprout_invitation = PMS.get_sprout_invitation!(id)
    {:ok, _sprout_invitation} = PMS.delete_sprout_invitation(sprout_invitation)

    conn
    |> put_flash(:info, "Sprout invitation deleted successfully.")
    |> redirect(to: pms_sprout_path(conn, :show, sprout_invitation.sprout))
  end
end
