defmodule SproutTreeWeb.Admin.PageController do
  use SproutTreeWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
