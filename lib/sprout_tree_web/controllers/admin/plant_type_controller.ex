defmodule SproutTreeWeb.Admin.PlantTypeController do
  use SproutTreeWeb, :controller

  alias SproutTree.PMS
  alias SproutTree.PMS.PlantType

  def index(conn, _params) do
    plant_types = PMS.list_plant_types()
    render(conn, "index.html", plant_types: plant_types)
  end

  def new(conn, _params) do
    changeset = PMS.change_plant_type(%PlantType{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"plant_type" => plant_type_params}) do
    case PMS.create_plant_type(plant_type_params) do
      {:ok, plant_type} ->
        conn
        |> put_flash(:info, "Plant type created successfully.")
        |> redirect(to: admin_plant_type_path(conn, :show, plant_type))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    plant_type = PMS.get_plant_type!(id)
    render(conn, "show.html", plant_type: plant_type)
  end

  def edit(conn, %{"id" => id}) do
    plant_type = PMS.get_plant_type!(id)
    changeset = PMS.change_plant_type(plant_type)
    render(conn, "edit.html", plant_type: plant_type, changeset: changeset)
  end

  def update(conn, %{"id" => id, "plant_type" => plant_type_params}) do
    plant_type = PMS.get_plant_type!(id)

    case PMS.update_plant_type(plant_type, plant_type_params) do
      {:ok, plant_type} ->
        conn
        |> put_flash(:info, "Plant type updated successfully.")
        |> redirect(to: admin_plant_type_path(conn, :show, plant_type))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", plant_type: plant_type, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    plant_type = PMS.get_plant_type!(id)
    {:ok, _plant_type} = PMS.delete_plant_type(plant_type)

    conn
    |> put_flash(:info, "Plant type deleted successfully.")
    |> redirect(to: admin_plant_type_path(conn, :index))
  end
end
