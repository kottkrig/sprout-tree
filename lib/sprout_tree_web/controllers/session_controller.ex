defmodule SproutTreeWeb.SessionController do
  use SproutTreeWeb, :controller

  alias SproutTree.Accounts

  def new(conn, _) do
    render(conn, "new.html")
  end

  def create(conn, %{"user" => %{"email" => email, "password" => password}}) do
    case Accounts.authenticate_by_email_password(email, password) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Välkommen tillbaka!")
        |> SproutTree.Accounts.Auth.Guardian.Plug.sign_in(user)
        |> redirect(to: "/")

      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> redirect(to: session_path(conn, :new))
    end
  end

  def delete(conn, _) do
    conn
    |> SproutTree.Accounts.Auth.Guardian.Plug.sign_out()
    |> redirect(to: "/")
  end
end
