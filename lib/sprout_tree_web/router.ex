defmodule SproutTreeWeb.Router do
  use SproutTreeWeb, :router

  defp maybe_add_user(conn, _) do
    maybe_user = Guardian.Plug.current_resource(conn)
    assign(conn, :maybe_user, maybe_user)
  end

  pipeline :auth do
    plug(SproutTree.Accounts.Auth.Pipeline)
    plug(:maybe_add_user)
  end

  pipeline :ensure_auth do
    plug(Guardian.Plug.EnsureAuthenticated)
  end

  pipeline :ensure_admin do
    plug(SproutTreeWeb.Auth.CheckAdmin)
  end

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", SproutTreeWeb do
    # Use the default browser stack
    pipe_through([:browser, :auth])

    get("/", PageController, :index)

    resources("/sessions", SessionController, only: [:new, :create, :delete], singleton: true)
    resources("/users", UserController, only: [:new, :create])
  end

  scope "/admin", SproutTreeWeb.Admin, as: :admin do
    pipe_through([:browser, :auth, :ensure_auth, :ensure_admin])

    get("/", PageController, :index)
    resources("/plant_types", PlantTypeController)
  end

  scope "/pms", SproutTreeWeb.PMS, as: :pms do
    pipe_through([:browser, :auth, :ensure_auth])

    resources("/users", UserController, only: [:show, :edit, :update, :delete])

    resources(
      "/sprout_requests",
      SproutRequestController,
      only: [:index, :update, :delete]
    )

    resources("/invitations", SproutInvitationController, only: [:delete])
    get("/invitations/:verification_code", SproutInvitationController, :show)
    post("/invitations/:id/accept", SproutInvitationController, :accept)

    post(
      "/invitations/:id/accept_to_existing_sprout",
      SproutInvitationController,
      :accept_to_existing_sprout
    )

    resources "/sprouts", SproutController do
      get("/invitations/generate", SproutInvitationController, :generate)
      get("/invitations/generate_parent", SproutInvitationController, :generate_parent)

      resources("/requests", SproutRequestController, only: [:new, :create])
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", SproutTreeWeb do
  #   pipe_through :api
  # end
end
