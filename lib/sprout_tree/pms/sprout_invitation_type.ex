import EctoEnum

defenum(
  SproutInvitationType,
  is_child: 0,
  is_parent: 1
)
