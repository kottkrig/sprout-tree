defmodule SproutTree.PMS do
  @moduledoc """
  The PMS context.
  """

  import Ecto.Query, warn: false
  alias SproutTree.Repo

  alias SproutTree.PMS
  alias SproutTree.PMS.Botanist

  @doc """
  Returns the list of botanists.

  ## Examples

      iex> list_botanists()
      [%Botanist{}, ...]

  """
  def list_botanists do
    Botanist
    |> Repo.all()
    |> Repo.preload([:user])
  end

  @doc """
  Gets a single botanist.

  Raises `Ecto.NoResultsError` if the Botanist does not exist.

  ## Examples

      iex> get_botanist!(123)
      %Botanist{}

      iex> get_botanist!(456)
      ** (Ecto.NoResultsError)

  """
  def get_botanist!(id) do
    Botanist
    |> Repo.get!(id)
    |> Repo.preload([:user])
  end

  @doc """
  Creates a botanist.

  ## Examples

      iex> ensure_botanist_exists(user)
      {:ok, %Botanist{}}

      iex> ensure_botanist_exists(not_a_user)
      {:error, %Ecto.Changeset{}}

  """
  def ensure_botanist_exists(%SproutTree.Accounts.User{} = user) do
    %Botanist{user_id: user.id}
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.unique_constraint(:user_id)
    |> Repo.insert()
    |> handle_existing_botanist()
  end

  defp handle_existing_botanist({:ok, botanist}), do: get_botanist!(botanist.id)

  defp handle_existing_botanist({:error, changeset}) do
    Botanist
    |> Repo.get_by!(user_id: changeset.data.user_id)
    |> Repo.preload([:user])
  end

  @doc """
  Updates a botanist.

  ## Examples

      iex> update_botanist(botanist, %{field: new_value})
      {:ok, %Botanist{}}

      iex> update_botanist(botanist, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_botanist(%Botanist{} = botanist, attrs) do
    botanist
    |> Botanist.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking botanist changes.

  ## Examples

      iex> change_botanist(botanist)
      %Ecto.Changeset{source: %Botanist{}}

  """
  def change_botanist(%Botanist{} = botanist) do
    Botanist.changeset(botanist, %{})
  end

  alias SproutTree.PMS.PlantType

  @doc """
  Returns the list of plant_types.

  ## Examples

      iex> list_plant_types()
      [%PlantType{}, ...]

  """
  def list_plant_types do
    Repo.all(PlantType)
  end

  @doc """
  Gets a single plant_type.

  Raises `Ecto.NoResultsError` if the Plant type does not exist.

  ## Examples

      iex> get_plant_type!(123)
      %PlantType{}

      iex> get_plant_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_plant_type!(id), do: Repo.get!(PlantType, id)

  @doc """
  Creates a plant_type.

  ## Examples

      iex> create_plant_type(%{field: value})
      {:ok, %PlantType{}}

      iex> create_plant_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_plant_type(attrs \\ %{}) do
    %PlantType{}
    |> PlantType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a plant_type.

  ## Examples

      iex> update_plant_type(plant_type, %{field: new_value})
      {:ok, %PlantType{}}

      iex> update_plant_type(plant_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_plant_type(%PlantType{} = plant_type, attrs) do
    plant_type
    |> PlantType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PlantType.

  ## Examples

      iex> delete_plant_type(plant_type)
      {:ok, %PlantType{}}

      iex> delete_plant_type(plant_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_plant_type(%PlantType{} = plant_type) do
    Repo.delete(plant_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking plant_type changes.

  ## Examples

      iex> change_plant_type(plant_type)
      %Ecto.Changeset{source: %PlantType{}}

  """
  def change_plant_type(%PlantType{} = plant_type) do
    PlantType.changeset(plant_type, %{})
  end

  alias SproutTree.PMS.Sprout

  @doc """
  Returns the list of botanist sprouts.

  ## Examples

      iex> list_sprouts(botanist)
      [%Sprout{}, ...]

  """
  def list_sprouts_for_botanist(%Botanist{} = botanist) do
    Sprout
    |> Sprout.for_botanist(botanist)
    |> Repo.all()
    |> Repo.preload([
      :plant_type,
      :sprouts,
      parent_sprout: [owner_botanist: [:user]],
      owner_botanist: [user: :credential]
    ])
  end

  @doc """
  Gets a single sprout.

  Raises `Ecto.NoResultsError` if the Sprout does not exist.

  ## Examples

      iex> get_sprout!(123)
      %Sprout{}

      iex> get_sprout!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sprout!(id) do
    Sprout
    |> Repo.get!(id)
    |> Repo.preload([:plant_type, owner_botanist: [user: :credential]])
    |> Sprout.load_parents()
    |> Sprout.load_children()
  end

  @doc """
  Creates a sprout.

  ## Examples

      iex> create_sprout(%{field: value})
      {:ok, %Sprout{}}

      iex> create_sprout(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_sprout(%Botanist{} = botanist, attrs \\ %{}) do
    %Sprout{}
    |> Sprout.changeset(attrs)
    |> Ecto.Changeset.put_change(:owner_botanist_id, botanist.id)
    |> Repo.insert()
  end

  @doc """
  Updates a sprout.

  ## Examples

      iex> update_sprout(sprout, %{field: new_value})
      {:ok, %Sprout{}}

      iex> update_sprout(sprout, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sprout(%Sprout{} = sprout, attrs) do
    sprout
    |> Sprout.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Sprout.

  ## Examples

      iex> delete_sprout(sprout)
      {:ok, %Sprout{}}

      iex> delete_sprout(sprout)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sprout(%Sprout{} = sprout) do
    Repo.delete(sprout)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sprout changes.

  ## Examples

      iex> change_sprout(sprout)
      %Ecto.Changeset{source: %Sprout{}}

  """
  def change_sprout(%Sprout{} = sprout) do
    Sprout.changeset(sprout, %{})
  end

  alias SproutTree.PMS.SproutRequest

  @doc """
  Returns the list of sprout_requests.

  ## Examples

      iex> list_sprout_requests_for_botanist(botanist)
      [%SproutRequest{}, ...]

  """
  def list_sprout_requests_for_botanist(%Botanist{} = botanist) do
    SproutRequest
    |> SproutRequest.for_botanist(botanist)
    |> SproutRequest.pending()
    |> Repo.all()
    |> Repo.preload(parent_sprout: [:plant_type], sender_botanist: [user: :credential])
  end

  @doc """
  Gets a single sprout_request.

  Raises `Ecto.NoResultsError` if the Sprout request does not exist.

  ## Examples

      iex> get_sprout_request!(123)
      %SproutRequest{}

      iex> get_sprout_request!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sprout_request!(id), do: Repo.get!(SproutRequest, id) |> Repo.preload([:parent_sprout])

  @doc """
  Creates a sprout_request.

  ## Examples

      iex> create_sprout_request(%{field: value})
      {:ok, %SproutRequest{}}

      iex> create_sprout_request(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_sprout_request(%Sprout{} = parent_sprout, attrs \\ %{}) do
    %SproutRequest{}
    |> SproutRequest.changeset(attrs)
    |> Ecto.Changeset.put_change(:sender_botanist_id, parent_sprout.owner_botanist.id)
    |> Ecto.Changeset.put_change(:parent_sprout_id, parent_sprout.id)
    |> Repo.insert()
  end

  @doc """
  Updates a sprout_request.

  ## Examples

      iex> update_sprout_request(sprout_request, %{field: new_value})
      {:ok, %SproutRequest{}}

      iex> update_sprout_request(sprout_request, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sprout_request(%SproutRequest{} = sprout_request, attrs) do
    changeset =
      sprout_request
      |> SproutRequest.changeset(attrs)

    new_status = changeset.changes[:status]

    if new_status == :accepted do
      receiver_botanist = get_botanist!(sprout_request.receiver_botanist_id)

      parent_sprout = PMS.get_sprout!(sprout_request.parent_sprout_id)

      _sprout =
        create_sprout(receiver_botanist, %{
          :name => SproutTree.Utils.PMS.new_sprout_name_from_parent_sprout(parent_sprout),
          :parent_sprout_id => sprout_request.parent_sprout_id,
          :plant_type_id => sprout_request.parent_sprout.plant_type_id
        })
    end

    Repo.update(changeset)
  end

  @doc """
  Deletes a SproutRequest.

  ## Examples

      iex> delete_sprout_request(sprout_request)
      {:ok, %SproutRequest{}}

      iex> delete_sprout_request(sprout_request)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sprout_request(%SproutRequest{} = sprout_request) do
    Repo.delete(sprout_request)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sprout_request changes.

  ## Examples

      iex> change_sprout_request(sprout_request)
      %Ecto.Changeset{source: %SproutRequest{}}

  """
  def change_sprout_request(%SproutRequest{} = sprout_request) do
    SproutRequest.changeset(sprout_request, %{})
  end

  alias SproutTree.PMS.SproutInvitation

  @doc """
  Returns the list of sprout_invitations.

  ## Examples

      iex> list_sprout_invitations()
      [%SproutInvitation{}, ...]

  """
  def list_sprout_invitations do
    SproutInvitation
    |> Repo.all()
    |> Repo.preload(sprout: [:plant_type, owner_botanist: [:user]])
  end

  def accept_sprout_invitation(
        %Botanist{} = botanist,
        %SproutInvitation{sprout: parent_sprout, new_sprout_type: :is_child} = sprout_invitation
      ) do
    sprout_changeset =
      %Sprout{}
      |> Sprout.changeset(%{
        :owner_botanist_id => botanist.id,
        :name => SproutTree.Utils.PMS.new_sprout_name_from_parent_sprout(parent_sprout),
        :parent_sprout_id => parent_sprout.id,
        :plant_type_id => parent_sprout.plant_type_id
      })

    Ecto.Multi.new()
    |> Ecto.Multi.insert(:sprout, sprout_changeset)
    |> Ecto.Multi.delete(:sprout_invitation, sprout_invitation)
    |> Repo.transaction()
    |> case do
      {:ok, %{sprout: sprout, sprout_invitation: _sprout_invitation}} ->
        {:ok, sprout}

      {:error, op, res, others} ->
        {:error, op}
    end
  end

  def accept_sprout_invitation(
        %Botanist{} = botanist,
        %SproutInvitation{sprout: existing_child_sprout, new_sprout_type: :is_parent} =
          sprout_invitation
      ) do
    new_sprout_changeset =
      %Sprout{}
      |> Sprout.changeset(%{
        :owner_botanist_id => botanist.id,
        :name => existing_child_sprout.plant_type.name,
        :plant_type_id => existing_child_sprout.plant_type_id
      })

    Ecto.Multi.new()
    |> Ecto.Multi.insert(:sprout, new_sprout_changeset)
    |> Ecto.Multi.run(:child_sprout, fn %{sprout: new_sprout} ->
      existing_child_sprout
      |> Sprout.changeset(%{
        parent_sprout_id: new_sprout.id
      })
      |> Repo.update()
    end)
    |> Ecto.Multi.delete(:sprout_invitation, sprout_invitation)
    |> Repo.transaction()
    |> case do
      {:ok, %{sprout: sprout, child_sprout: _child_sprout, sprout_invitation: _sprout_invitation}} ->
        {:ok, sprout}

      {:error, op, res, others} ->
        {:error, op}
    end
  end

  def accept_sprout_invitation_to_existing_sprout(
        %Botanist{} = botanist,
        %SproutInvitation{sprout: parent_sprout, new_sprout_type: :is_child} = sprout_invitation,
        %Sprout{} = existing_sprout
      ) do
    sprout_changeset =
      existing_sprout
      |> Sprout.changeset(%{
        :parent_sprout_id => parent_sprout.id
      })

    Ecto.Multi.new()
    |> Ecto.Multi.update(:sprout, sprout_changeset)
    |> Ecto.Multi.delete(:sprout_invitation, sprout_invitation)
    |> Repo.transaction()
    |> case do
      {:ok, %{sprout: sprout, sprout_invitation: _sprout_invitation}} ->
        {:ok, sprout}

      {:error, op, res, others} ->
        {:error, op}
    end
  end

  def accept_sprout_invitation_to_existing_sprout(
        %Botanist{} = botanist,
        %SproutInvitation{sprout: child_sprout, new_sprout_type: :is_parent} = sprout_invitation,
        %Sprout{} = existing_sprout
      ) do
    sprout_changeset =
      child_sprout
      |> Sprout.changeset(%{
        :parent_sprout_id => existing_sprout.id
      })

    Ecto.Multi.new()
    |> Ecto.Multi.update(:sprout, sprout_changeset)
    |> Ecto.Multi.delete(:sprout_invitation, sprout_invitation)
    |> Repo.transaction()
    |> case do
      {:ok, %{sprout: sprout, sprout_invitation: _sprout_invitation}} ->
        {:ok, existing_sprout}

      {:error, op, res, others} ->
        {:error, op}
    end
  end

  @doc """
  Gets a single sprout_invitation.

  Raises `Ecto.NoResultsError` if the Sprout invitation does not exist.

  ## Examples

      iex> get_sprout_invitation!(123)
      %SproutInvitation{}

      iex> get_sprout_invitation!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sprout_invitation!(id) do
    SproutInvitation
    |> Repo.get!(id)
    |> Repo.preload(sprout: [:plant_type, owner_botanist: [:user]])
  end

  def get_sprout_invitation_with_verification_code!(verification_code) do
    SproutInvitation
    |> SproutInvitation.for_verification_code(verification_code)
    |> Repo.one!()
    |> Repo.preload(sprout: [:plant_type, owner_botanist: [:user]])
  end

  @doc """
  Generate a child sprout invitation.

  ## Examples

      iex> generate_child_sprout_invitation(%{field: value})
      {:ok, %SproutInvitation{}}

      iex> generate_child_sprout_invitation(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def generate_child_sprout_invitation(%Sprout{} = sprout, attrs \\ %{}) do
    attrs =
      Map.merge(attrs, %{
        :new_sprout_type => :is_child,
        :verification_code => SproutTree.Utils.PMS.invitation_verification_code(6),
        :sprout_id => sprout.id
      })

    %SproutInvitation{}
    |> SproutInvitation.changeset(attrs)
    |> Repo.insert()
    |> handle_existing_child_sprout_invitation()
  end

  defp handle_existing_child_sprout_invitation({:ok, sprout_invitation}),
    do: {:ok, sprout_invitation}

  defp handle_existing_child_sprout_invitation({:error, changeset}) do
    sprout =
      changeset
      |> Ecto.Changeset.get_field(:sproud_id)
      |> get_sprout!

    generate_child_sprout_invitation(sprout)
  end

  def generate_parent_sprout_invitation(%Sprout{} = sprout, attrs \\ %{}) do
    attrs =
      Map.merge(attrs, %{
        :new_sprout_type => :is_parent,
        :verification_code => SproutTree.Utils.PMS.invitation_verification_code(6),
        :sprout_id => sprout.id
      })

    %SproutInvitation{}
    |> SproutInvitation.changeset(attrs)
    |> Repo.insert()
    |> handle_existing_parent_sprout_invitation()
  end

  defp handle_existing_parent_sprout_invitation({:ok, sprout_invitation}),
    do: {:ok, sprout_invitation}

  defp handle_existing_parent_sprout_invitation({:error, changeset}) do
    sprout =
      changeset
      |> Ecto.Changeset.get_field(:sproud_id)
      |> get_sprout!

    generate_parent_sprout_invitation(sprout)
  end

  @doc """
  Updates a sprout_invitation.

  ## Examples

      iex> update_sprout_invitation(sprout_invitation, %{field: new_value})
      {:ok, %SproutInvitation{}}

      iex> update_sprout_invitation(sprout_invitation, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sprout_invitation(%SproutInvitation{} = sprout_invitation, attrs) do
    sprout_invitation
    |> SproutInvitation.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a SproutInvitation.

  ## Examples

      iex> delete_sprout_invitation(sprout_invitation)
      {:ok, %SproutInvitation{}}

      iex> delete_sprout_invitation(sprout_invitation)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sprout_invitation(%SproutInvitation{} = sprout_invitation) do
    Repo.delete(sprout_invitation)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sprout_invitation changes.

  ## Examples

      iex> change_sprout_invitation(sprout_invitation)
      %Ecto.Changeset{source: %SproutInvitation{}}

  """
  def change_sprout_invitation(%SproutInvitation{} = sprout_invitation) do
    SproutInvitation.changeset(sprout_invitation, %{})
  end
end
