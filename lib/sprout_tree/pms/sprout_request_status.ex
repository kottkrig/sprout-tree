import EctoEnum

defenum(
  SproutRequestStatus,
  pending: 0,
  accepted: 1,
  declined: 2
)
