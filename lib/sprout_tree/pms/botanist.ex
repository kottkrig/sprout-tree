alias SproutTree.PMS.{Botanist, Sprout}
alias SproutTree.Accounts.User

defmodule SproutTree.PMS.Botanist do
  use Ecto.Schema
  import Ecto.Changeset

  schema "botanists" do
    field(:bio, :string)
    belongs_to(:user, User)

    has_many(:sprouts, Sprout, foreign_key: :owner_botanist_id)

    timestamps()
  end

  @doc false
  def changeset(botanist, attrs) do
    botanist
    |> cast(attrs, [:bio, :user_id])
    |> validate_required([:user_id])
  end
end
