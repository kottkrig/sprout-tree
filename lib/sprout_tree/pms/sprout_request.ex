alias SproutTree.PMS.{Botanist, Sprout}

defmodule SproutTree.PMS.SproutRequest do
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  schema "sprout_requests" do
    field(:message, :string)
    field(:status, SproutRequestStatus)

    belongs_to(:sender_botanist, Botanist)
    belongs_to(:receiver_botanist, Botanist)
    belongs_to(:parent_sprout, Sprout)

    timestamps()
  end

  def pending(query) do
    from(sprout_request in query, where: sprout_request.status == ^:pending)
  end

  def for_botanist(query, %Botanist{} = botanist) do
    from(sprout_request in query, where: sprout_request.receiver_botanist_id == ^botanist.id)
  end

  @doc false
  def changeset(sprout_request, attrs) do
    sprout_request
    |> cast(attrs, [:message, :status, :receiver_botanist_id])
    |> validate_required([])
  end
end
