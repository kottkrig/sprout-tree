defmodule SproutTree.PMS.Sprout do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias SproutTree.PMS.{Sprout, PlantType, Botanist}
  alias SproutTree.Repo

  schema "sprouts" do
    field(:name, :string)
    field(:description, :string)

    belongs_to(:owner_botanist, Botanist)
    belongs_to(:parent_sprout, Sprout)
    belongs_to(:plant_type, PlantType)

    has_many(:sprouts, Sprout, foreign_key: :parent_sprout_id)

    timestamps()
  end

  def for_botanist(query, %Botanist{} = botanist) do
    from(sprout in query, where: sprout.owner_botanist_id == ^botanist.id)
  end

  @doc """
    Recursively loads parents into the given struct until it hits nil
  """
  def load_parents(parent_sprout) do
    load_parents(parent_sprout, 10)
  end

  def load_parents(_, limit) when limit < 0, do: raise("Recursion limit reached")

  def load_parents(%Sprout{parent_sprout: nil} = parent_sprout, _), do: parent_sprout

  def load_parents(%Sprout{parent_sprout: %Ecto.Association.NotLoaded{}} = parent_sprout, limit) do
    parent_sprout =
      Repo.preload(
        parent_sprout,
        parent_sprout: [owner_botanist: [:user]]
      )

    Map.update!(parent_sprout, :parent_sprout, &load_parents(&1, limit - 1))
  end

  def load_parents(nil, _), do: nil

  @doc """
    Recursively loads children into the given struct until it hits []
  """
  def load_children(sprout), do: load_children(sprout, 10)

  def load_children(_, limit) when limit < 0, do: raise("Recursion limit reached")

  def load_children(%Sprout{sprouts: %Ecto.Association.NotLoaded{}} = sprout, limit) do
    # maybe include a custom query here to preserve some order
    sprout =
      sprout
      |> Repo.preload(sprouts: [owner_botanist: [:user]])

    Map.update!(sprout, :sprouts, fn list ->
      Enum.map(list, &Sprout.load_children(&1, limit - 1))
    end)
  end

  @doc false
  def changeset(sprout, attrs) do
    sprout
    |> cast(attrs, [:description, :name, :owner_botanist_id, :plant_type_id, :parent_sprout_id])
    |> cast_assoc(:plant_type)
    |> validate_required([])
  end
end
