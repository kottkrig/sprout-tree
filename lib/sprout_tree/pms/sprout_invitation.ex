defmodule SproutTree.PMS.SproutInvitation do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias SproutTree.PMS.Sprout

  schema "sprout_invitations" do
    field(:new_sprout_type, SproutInvitationType)
    field(:verification_code, :string)

    belongs_to(:sprout, Sprout)

    timestamps()
  end

  def for_verification_code(query, verification_code) do
    from(
      sprout_invitation in query,
      where: sprout_invitation.verification_code == ^verification_code
    )
  end

  @doc false
  def changeset(sprout_invitation, attrs) do
    sprout_invitation
    |> cast(attrs, [:verification_code, :new_sprout_type, :sprout_id])
    |> validate_required([:verification_code, :new_sprout_type])
    |> unique_constraint(:verification_code)
  end
end
