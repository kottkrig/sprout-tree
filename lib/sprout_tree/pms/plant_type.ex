defmodule SproutTree.PMS.PlantType do
  use Ecto.Schema
  import Ecto.Changeset


  schema "plant_types" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(plant_type, attrs) do
    plant_type
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
