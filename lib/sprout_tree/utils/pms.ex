defmodule SproutTree.Utils.PMS do
  alias SproutTree.Accounts.{User}
  alias SproutTree.PMS.{Botanist, PlantType, Sprout}

  def new_sprout_name_from_parent_sprout(%Sprout{
        plant_type: %PlantType{name: plant_type_name},
        owner_botanist: %Botanist{user: %User{first_name: user_first_name}}
      }) do
    "#{plant_type_name} från #{user_first_name}"
  end

  def new_sprout_name_from_parent_sprout(%Sprout{
        plant_type: %PlantType{name: plant_type_name}
      }) do
    "#{plant_type_name}"
  end

  def invitation_verification_code(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end
end
