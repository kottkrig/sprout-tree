alias SproutTree.Accounts.{User, Credential}

defmodule SproutTree.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:first_name, :string)
    field(:full_name, :string)
    field(:is_admin, :boolean, default: false)

    has_one(:credential, Credential)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:first_name, :full_name, :is_admin])
    |> validate_required([:first_name, :full_name])
  end
end
