defmodule SproutTreeWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      import SproutTreeWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint SproutTreeWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(SproutTree.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(SproutTree.Repo, {:shared, self()})
    end

    {conn, user} =
      cond do
        tags[:authenticated_admin] ->
          admin = create_admin()

          conn =
            Phoenix.ConnTest.build_conn()
            |> Plug.Test.init_test_session(%{})
            |> SproutTree.Accounts.Auth.Guardian.Plug.sign_in(admin)

          {conn, admin}

        tags[:authenticated_user] ->
          user = create_user()

          conn =
            Phoenix.ConnTest.build_conn()
            |> Plug.Test.init_test_session(%{})
            |> SproutTree.Accounts.Auth.Guardian.Plug.sign_in(user)

          {conn, user}

        true ->
          {Phoenix.ConnTest.build_conn(), nil}
      end

    {:ok, conn: conn, user: user}
  end

  defp create_user do
    {:ok, user} =
      SproutTree.Accounts.create_user(%{
        first_name: "some first name",
        full_name: "some full name",
        credential: %{
          email: "test@test.com",
          password: "1234"
        }
      })

    user
  end

  defp create_admin do
    {:ok, user} =
      SproutTree.Accounts.create_user(%{
        first_name: "admin first name",
        full_name: "admin full name",
        is_admin: true,
        credential: %{
          email: "admin@test.com",
          password: "1234"
        }
      })

    user
  end
end
