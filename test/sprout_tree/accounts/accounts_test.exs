defmodule SproutTree.AccountsTest do
  use SproutTree.DataCase

  alias SproutTree.Accounts

  describe "users" do
    alias SproutTree.Accounts.User

    @valid_attrs %{
      first_name: "some first_name",
      full_name: "some full_name",
      credential: %{email: "test@test.com", password: "1234"}
    }

    @update_attrs %{
      first_name: "some updated first_name",
      full_name: "some updated full_name"
    }

    @invalid_attrs %{email: nil, first_name: nil, full_name: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.credential.email == "test@test.com"
      assert user.first_name == "some first_name"
      assert user.full_name == "some full_name"
    end

    test "create_user/1 encrypts the password" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      refute user.credential.password == "1234"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.first_name == "some updated first_name"
      assert user.full_name == "some updated full_name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user and credential" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end

      assert_raise Ecto.NoResultsError, fn ->
        Repo.get!(Accounts.Credential, user.credential.id)
      end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
