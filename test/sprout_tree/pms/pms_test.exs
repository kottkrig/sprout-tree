defmodule SproutTree.PMSTest do
  use SproutTree.DataCase

  alias SproutTree.Accounts
  alias SproutTree.PMS

  setup do
    {:ok, user} =
      Accounts.create_user(%{
        first_name: "Test",
        full_name: "Test Testersson",
        credential: %{
          email: "test@test.com",
          password: "1234"
        }
      })

    botanist = PMS.ensure_botanist_exists(user)

    [botanist: botanist]
  end

  describe "botanists" do
    alias SproutTree.PMS.Botanist

    @valid_attrs %{bio: "some bio"}
    @update_attrs %{bio: "some updated bio"}
    @invalid_attrs %{user_id: nil}

    test "list_botanists/0 returns all botanists", %{botanist: botanist} do
      assert PMS.list_botanists() == [botanist]
    end

    test "get_botanist!/1 returns the botanist with given id", %{botanist: botanist} do
      assert PMS.get_botanist!(botanist.id) == botanist
    end

    test "create_botanist/1 with valid data creates a botanist" do
      {:ok, user} =
        Accounts.create_user(%{
          first_name: "Test",
          full_name: "Test Testersson",
          credential: %{
            email: "test2@test.com",
            password: "1234"
          }
        })

      botanist = PMS.ensure_botanist_exists(user)

      assert botanist.user_id == user.id
    end

    test "update_botanist/2 with valid data updates the botanist", %{botanist: botanist} do
      assert {:ok, botanist} = PMS.update_botanist(botanist, @update_attrs)
      assert %Botanist{} = botanist
      assert botanist.bio == "some updated bio"
    end

    test "update_botanist/2 with invalid data returns error changeset", %{botanist: botanist} do
      assert {:error, %Ecto.Changeset{}} = PMS.update_botanist(botanist, @invalid_attrs)
      assert botanist == PMS.get_botanist!(botanist.id)
    end

    test "delete_user/1 deletes the associated botanist", %{botanist: botanist} do
      assert {:ok, %Accounts.User{}} = Accounts.delete_user(botanist.user)
      assert_raise Ecto.NoResultsError, fn -> PMS.get_botanist!(botanist.id) end
    end

    test "change_botanist/1 returns a botanist changeset", %{botanist: botanist} do
      assert %Ecto.Changeset{} = PMS.change_botanist(botanist)
    end
  end

  describe "plant_types" do
    alias SproutTree.PMS.PlantType

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def plant_type_fixture(attrs \\ %{}) do
      {:ok, plant_type} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PMS.create_plant_type()

      plant_type
    end

    test "list_plant_types/0 returns all plant_types" do
      plant_type = plant_type_fixture()
      assert PMS.list_plant_types() == [plant_type]
    end

    test "get_plant_type!/1 returns the plant_type with given id" do
      plant_type = plant_type_fixture()
      assert PMS.get_plant_type!(plant_type.id) == plant_type
    end

    test "create_plant_type/1 with valid data creates a plant_type" do
      assert {:ok, %PlantType{} = plant_type} = PMS.create_plant_type(@valid_attrs)
      assert plant_type.name == "some name"
    end

    test "create_plant_type/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PMS.create_plant_type(@invalid_attrs)
    end

    test "update_plant_type/2 with valid data updates the plant_type" do
      plant_type = plant_type_fixture()
      assert {:ok, plant_type} = PMS.update_plant_type(plant_type, @update_attrs)
      assert %PlantType{} = plant_type
      assert plant_type.name == "some updated name"
    end

    test "update_plant_type/2 with invalid data returns error changeset" do
      plant_type = plant_type_fixture()
      assert {:error, %Ecto.Changeset{}} = PMS.update_plant_type(plant_type, @invalid_attrs)
      assert plant_type == PMS.get_plant_type!(plant_type.id)
    end

    test "delete_plant_type/1 deletes the plant_type" do
      plant_type = plant_type_fixture()
      assert {:ok, %PlantType{}} = PMS.delete_plant_type(plant_type)
      assert_raise Ecto.NoResultsError, fn -> PMS.get_plant_type!(plant_type.id) end
    end

    test "change_plant_type/1 returns a plant_type changeset" do
      plant_type = plant_type_fixture()
      assert %Ecto.Changeset{} = PMS.change_plant_type(plant_type)
    end
  end

  #
  describe "sprouts" do
    alias SproutTree.PMS.Sprout

    @valid_attrs %{name: "some name", description: "some description"}
    @update_attrs %{}
    @invalid_attrs %{}

    def sprout_fixture(botanist, attrs \\ %{}) do
      {:ok, sprout} = PMS.create_sprout(botanist, attrs)

      PMS.get_sprout!(sprout.id)
    end

    test "list_sprouts/0 returns all sprouts", %{botanist: botanist} do
      sprout = sprout_fixture(botanist)
      assert PMS.list_sprouts_for_botanist(botanist) == [sprout]
    end

    test "get_sprout!/1 returns the sprout with given id", %{botanist: botanist} do
      sprout = sprout_fixture(botanist)
      assert PMS.get_sprout!(sprout.id) == sprout
    end

    test "create_sprout/1 with valid data creates a sprout", %{botanist: botanist} do
      assert {:ok, %Sprout{} = sprout} = PMS.create_sprout(botanist, @valid_attrs)
    end

    # test "create_sprout/1 with invalid data returns error changeset" do
    #   assert {:error, %Ecto.Changeset{}} = PMS.create_sprout(@invalid_attrs)
    # end
    #

    test "update_sprout/2 with valid data updates the sprout", %{botanist: botanist} do
      sprout = sprout_fixture(botanist)
      assert {:ok, sprout} = PMS.update_sprout(sprout, @update_attrs)
      assert %Sprout{} = sprout
    end

    #
    # test "update_sprout/2 with invalid data returns error changeset" do
    #   sprout = sprout_fixture()
    #   assert {:error, %Ecto.Changeset{}} = PMS.update_sprout(sprout, @invalid_attrs)
    #   assert sprout == PMS.get_sprout!(sprout.id)
    # end

    test "delete_sprout/1 deletes the sprout", %{botanist: botanist} do
      sprout = sprout_fixture(botanist)
      assert {:ok, %Sprout{}} = PMS.delete_sprout(sprout)
      assert_raise Ecto.NoResultsError, fn -> PMS.get_sprout!(sprout.id) end
    end

    test "change_sprout/1 returns a sprout changeset", %{botanist: botanist} do
      sprout = sprout_fixture(botanist)
      assert %Ecto.Changeset{} = PMS.change_sprout(sprout)
    end
  end

  describe "sprout_invitations" do
    alias SproutTree.PMS.SproutInvitation

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def sprout_invitation_fixture(attrs \\ %{}) do
      {:ok, sprout_invitation} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PMS.create_sprout_invitation()

      sprout_invitation
    end

    def child_sprout_invitation_fixture(botanist) do
      {:ok, sprout} = PMS.create_sprout(botanist, %{plant_type: %{name: "some plant type"}})
      {:ok, sprout_invitation} = PMS.generate_child_sprout_invitation(sprout)

      PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
    end

    def parent_sprout_invitation_fixture(botanist) do
      {:ok, sprout} = PMS.create_sprout(botanist, %{plant_type: %{name: "some plant type"}})
      {:ok, sprout_invitation} = PMS.generate_parent_sprout_invitation(sprout)

      PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
    end

    test "list_sprout_invitations/0 returns all sprout_invitations", %{botanist: botanist} do
      sprout_invitation = child_sprout_invitation_fixture(botanist)

      assert PMS.list_sprout_invitations() == [sprout_invitation]
    end

    test "get_sprout_invitation_with_verification_code!/1 returns the sprout_invitation with given verification code",
         %{
           botanist: botanist
         } do
      sprout_invitation = child_sprout_invitation_fixture(botanist)

      assert PMS.get_sprout_invitation_with_verification_code!(
               sprout_invitation.verification_code
             ) == sprout_invitation
    end

    test "generate_child_sprout_invitation/1 creates a sprout_invitation", %{
      botanist: botanist
    } do
      {:ok, sprout} = PMS.create_sprout(botanist, %{})

      assert {:ok, %SproutInvitation{} = sprout_invitation} =
               PMS.generate_child_sprout_invitation(sprout)

      assert sprout_invitation.new_sprout_type == :is_child
    end

    test "generate_parent_sprout_invitation/1 creates a sprout_invitation", %{
      botanist: botanist
    } do
      {:ok, sprout} = PMS.create_sprout(botanist, %{})

      assert {:ok, %SproutInvitation{} = sprout_invitation} =
               PMS.generate_parent_sprout_invitation(sprout)

      assert sprout_invitation.new_sprout_type == :is_parent
    end

    test "accept_sprout_invitation/1 creates a new child sprout", %{
      botanist: botanist
    } do
      sprout_invitation = child_sprout_invitation_fixture(botanist)

      assert {:ok, %PMS.Sprout{} = child_sprout} =
               PMS.accept_sprout_invitation(botanist, sprout_invitation)

      assert child_sprout.parent_sprout_id == sprout_invitation.sprout.id

      assert_raise Ecto.NoResultsError, fn ->
        PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
      end
    end

    test "accept_sprout_invitation/1 creates a new parent sprout", %{
      botanist: botanist
    } do
      sprout_invitation = parent_sprout_invitation_fixture(botanist)

      assert {:ok, %PMS.Sprout{} = parent_sprout} =
               PMS.accept_sprout_invitation(botanist, sprout_invitation)

      sprout = PMS.get_sprout!(sprout_invitation.sprout.id)

      assert sprout.parent_sprout_id == parent_sprout.id

      assert_raise Ecto.NoResultsError, fn ->
        PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
      end
    end

    test "accept_sprout_invitation_to_existing_sprout/1 merges with existing child sprout", %{
      botanist: botanist
    } do
      sprout_invitation = child_sprout_invitation_fixture(botanist)
      {:ok, existing_sprout} = PMS.create_sprout(botanist, %{name: "existing sprout"})

      assert {:ok, %PMS.Sprout{} = child_sprout} =
               PMS.accept_sprout_invitation_to_existing_sprout(
                 botanist,
                 sprout_invitation,
                 existing_sprout
               )

      existing_sprout = PMS.get_sprout!(existing_sprout.id)

      assert existing_sprout.parent_sprout_id == sprout_invitation.sprout.id

      assert_raise Ecto.NoResultsError, fn ->
        PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
      end
    end

    test "accept_sprout_invitation_to_existing_sprout/1 merges with existing parent sprout", %{
      botanist: botanist
    } do
      sprout_invitation = parent_sprout_invitation_fixture(botanist)
      {:ok, existing_sprout} = PMS.create_sprout(botanist, %{name: "existing sprout"})

      assert {:ok, %PMS.Sprout{} = parent_sprout} =
               PMS.accept_sprout_invitation_to_existing_sprout(
                 botanist,
                 sprout_invitation,
                 existing_sprout
               )

      sprout = PMS.get_sprout!(sprout_invitation.sprout.id)

      assert sprout.parent_sprout_id == existing_sprout.id

      assert_raise Ecto.NoResultsError, fn ->
        PMS.get_sprout_invitation_with_verification_code!(sprout_invitation.verification_code)
      end
    end
  end

  # describe "sprout_requests" do
  #   alias SproutTree.PMS.SproutRequest
  #
  #   @valid_attrs %{message: "some message", status: 42, status: :pending}
  #   @update_attrs %{message: "some updated message", status: 43}
  #   @invalid_attrs %{message: nil, status: nil}
  #
  #   def sprout_request_fixture(%PMS.Botanist{} = botanist, attrs \\ %{}) do
  #     {:ok, sprout} = PMS.create_sprout(botanist, %{})
  #     sprout = PMS.get_sprout!(sprout.id)
  #     {:ok, sprout_request} = PMS.create_sprout_request(sprout, Enum.into(attrs, @valid_attrs))
  #
  #     sprout_request
  #   end
  #
  #   test "list_sprout_requests/0 returns all sprout_requests", %{botanist: botanist} do
  #     sprout_request = sprout_request_fixture(botanist)
  #     assert PMS.list_sprout_requests_for_botanist(botanist) == [sprout_request]
  #   end

  # test "get_sprout_request!/1 returns the sprout_request with given id" do
  #   sprout_request = sprout_request_fixture()
  #   assert PMS.get_sprout_request!(sprout_request.id) == sprout_request
  # end
  #
  # test "create_sprout_request/1 with valid data creates a sprout_request" do
  #   assert {:ok, %SproutRequest{} = sprout_request} = PMS.create_sprout_request(@valid_attrs)
  #   assert sprout_request.message == "some message"
  #   assert sprout_request.status == 42
  # end
  #
  # test "create_sprout_request/1 with invalid data returns error changeset" do
  #   assert {:error, %Ecto.Changeset{}} = PMS.create_sprout_request(@invalid_attrs)
  # end
  #
  # test "update_sprout_request/2 with valid data updates the sprout_request" do
  #   sprout_request = sprout_request_fixture()
  #   assert {:ok, sprout_request} = PMS.update_sprout_request(sprout_request, @update_attrs)
  #   assert %SproutRequest{} = sprout_request
  #   assert sprout_request.message == "some updated message"
  #   assert sprout_request.status == 43
  # end
  #
  # test "update_sprout_request/2 with invalid data returns error changeset" do
  #   sprout_request = sprout_request_fixture()
  #
  #   assert {:error, %Ecto.Changeset{}} =
  #            PMS.update_sprout_request(sprout_request, @invalid_attrs)
  #
  #   assert sprout_request == PMS.get_sprout_request!(sprout_request.id)
  # end
  #
  # test "delete_sprout_request/1 deletes the sprout_request" do
  #   sprout_request = sprout_request_fixture()
  #   assert {:ok, %SproutRequest{}} = PMS.delete_sprout_request(sprout_request)
  #   assert_raise Ecto.NoResultsError, fn -> PMS.get_sprout_request!(sprout_request.id) end
  # end
  #
  # test "change_sprout_request/1 returns a sprout_request changeset" do
  #   sprout_request = sprout_request_fixture()
  #   assert %Ecto.Changeset{} = PMS.change_sprout_request(sprout_request)
  # end
  # end
end
