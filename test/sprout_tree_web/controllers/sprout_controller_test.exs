defmodule SproutTreeWeb.SproutControllerTest do
  use SproutTreeWeb.ConnCase

  alias SproutTree.PMS
  alias SproutTree.Accounts

  @create_attrs %{name: "some name", description: "some description"}
  @update_attrs %{name: "some updated name", description: "some updated description"}
  @invalid_attrs %{}

  def fixture(:sprout, user) do
    botanist = PMS.ensure_botanist_exists(user)
    {:ok, sprout} = PMS.create_sprout(botanist, @create_attrs)

    sprout
  end

  @tag :authenticated_user
  describe "index" do
    test "lists all sprouts", %{conn: conn} do
      conn = get(conn, pms_sprout_path(conn, :index))
      assert html_response(conn, 200) =~ "Mina växter"
    end
  end

  @tag :authenticated_user
  describe "new sprout" do
    test "renders form", %{conn: conn} do
      conn = get(conn, pms_sprout_path(conn, :new))
      assert html_response(conn, 200) =~ "Lägg till en ny planta"
    end
  end

  @tag :authenticated_user
  describe "create sprout" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, pms_sprout_path(conn, :create), sprout: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == pms_sprout_path(conn, :show, id)

      conn = get(conn, pms_sprout_path(conn, :show, id))
      assert html_response(conn, 200) =~ "some name"
    end

    # test "renders errors when data is invalid", %{conn: conn} do
    #   conn = post(conn, pms_sprout_path(conn, :create), sprout: @invalid_attrs)
    #   assert html_response(conn, 200) =~ "New Sprout"
    # end
  end

  @tag :authenticated_user
  describe "edit sprout" do
    setup [:create_sprout]

    test "renders form for editing chosen sprout", %{conn: conn, sprout: sprout} do
      conn = get(conn, pms_sprout_path(conn, :edit, sprout))
      assert html_response(conn, 200) =~ "Ändra \"some name\""
    end
  end

  @tag :authenticated_user
  describe "update sprout" do
    setup [:create_sprout]

    test "redirects when data is valid", %{conn: conn, sprout: sprout} do
      conn = put(conn, pms_sprout_path(conn, :update, sprout), sprout: @update_attrs)
      assert redirected_to(conn) == pms_sprout_path(conn, :show, sprout)

      conn = get(conn, pms_sprout_path(conn, :show, sprout))
      assert html_response(conn, 200)
    end

    # test "renders errors when data is invalid", %{conn: conn, sprout: sprout} do
    #   conn = put(conn, pms_sprout_path(conn, :update, sprout), sprout: @invalid_attrs)
    #   assert html_response(conn, 200) =~ "Edit Sprout"
    # end
  end

  @tag :authenticated_user
  describe "delete sprout" do
    setup [:create_sprout]

    test "deletes chosen sprout", %{conn: conn, sprout: sprout} do
      conn = delete(conn, pms_sprout_path(conn, :delete, sprout))
      assert redirected_to(conn) == pms_sprout_path(conn, :index)

      assert_error_sent(404, fn ->
        get(conn, pms_sprout_path(conn, :show, sprout))
      end)
    end
  end

  defp create_sprout(%{user: %SproutTree.Accounts.User{} = user}) do
    sprout = fixture(:sprout, user)
    {:ok, sprout: sprout}
  end
end
