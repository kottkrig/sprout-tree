defmodule SproutTreeWeb.PageControllerTest do
  use SproutTreeWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Välkommen till Sticklingsträdet"
  end
end
