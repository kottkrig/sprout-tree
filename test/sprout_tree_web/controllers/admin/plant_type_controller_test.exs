defmodule SproutTreeWeb.Admin.PlantTypeControllerTest do
  use SproutTreeWeb.ConnCase

  alias SproutTree.PMS

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:plant_type) do
    {:ok, plant_type} = PMS.create_plant_type(@create_attrs)
    plant_type
  end

  describe "user access" do
    @tag :authenticated_user
    test "user should not get access to admin area", %{conn: conn} do
      conn = get(conn, admin_plant_type_path(conn, :index))
      assert html_response(conn, 404) =~ "Not Found"
    end
  end

  describe "index" do
    @tag :authenticated_admin
    test "lists all plant_types", %{conn: conn} do
      conn = get(conn, admin_plant_type_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Plant types"
    end
  end

  describe "new plant_type" do
    @tag :authenticated_admin
    test "renders form", %{conn: conn} do
      conn = get(conn, admin_plant_type_path(conn, :new))
      assert html_response(conn, 200) =~ "New Plant type"
    end
  end

  describe "create plant_type" do
    @tag :authenticated_admin
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, admin_plant_type_path(conn, :create), plant_type: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == admin_plant_type_path(conn, :show, id)

      conn = get(conn, admin_plant_type_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Plant type"
    end

    @tag :authenticated_admin
    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, admin_plant_type_path(conn, :create), plant_type: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Plant type"
    end
  end

  describe "edit plant_type" do
    setup [:create_plant_type]

    @tag :authenticated_admin
    test "renders form for editing chosen plant_type", %{conn: conn, plant_type: plant_type} do
      conn = get(conn, admin_plant_type_path(conn, :edit, plant_type))
      assert html_response(conn, 200) =~ "Edit Plant type"
    end
  end

  describe "update plant_type" do
    setup [:create_plant_type]

    @tag :authenticated_admin
    test "redirects when data is valid", %{conn: conn, plant_type: plant_type} do
      conn =
        put(conn, admin_plant_type_path(conn, :update, plant_type), plant_type: @update_attrs)

      assert redirected_to(conn) == admin_plant_type_path(conn, :show, plant_type)

      conn = get(conn, admin_plant_type_path(conn, :show, plant_type))
      assert html_response(conn, 200) =~ "some updated name"
    end

    @tag :authenticated_admin
    test "renders errors when data is invalid", %{conn: conn, plant_type: plant_type} do
      conn =
        put(conn, admin_plant_type_path(conn, :update, plant_type), plant_type: @invalid_attrs)

      assert html_response(conn, 200) =~ "Edit Plant type"
    end
  end

  describe "delete plant_type" do
    setup [:create_plant_type]

    @tag :authenticated_admin
    test "deletes chosen plant_type", %{conn: conn, plant_type: plant_type} do
      conn = delete(conn, admin_plant_type_path(conn, :delete, plant_type))
      assert redirected_to(conn) == admin_plant_type_path(conn, :index)

      assert_error_sent(404, fn ->
        get(conn, admin_plant_type_path(conn, :show, plant_type))
      end)
    end
  end

  defp create_plant_type(_) do
    plant_type = fixture(:plant_type)
    {:ok, plant_type: plant_type}
  end
end
