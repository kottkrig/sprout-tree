defmodule SproutTreeWeb.SproutInvitationControllerTest do
  use SproutTreeWeb.ConnCase

  alias SproutTree.PMS

  @create_attrs %{new_sprout_type: 42, verification_code: "some verification_code"}
  @update_attrs %{new_sprout_type: 43, verification_code: "some updated verification_code"}
  @invalid_attrs %{new_sprout_type: nil, verification_code: nil}

  def fixture(:child_sprout_invitation, user) do
    botanist = PMS.ensure_botanist_exists(user)
    {:ok, sprout} = PMS.create_sprout(botanist, %{name: "some sprout name"})
    {:ok, sprout_invitation} = PMS.generate_child_sprout_invitation(sprout)
    sprout_invitation
  end

  def fixture(:sprout, user) do
    botanist = PMS.ensure_botanist_exists(user)
    {:ok, sprout} = PMS.create_sprout(botanist, %{name: "some sprout name"})
    sprout
  end

  describe "generate sprout_invitation" do
    setup [:create_sprout]

    @tag :authenticated_user
    test "redirects to show when child sprout invitation is generated", %{
      conn: conn,
      sprout: sprout
    } do
      conn = get(conn, pms_sprout_sprout_invitation_path(conn, :generate, sprout))

      assert %{verification_code: verification_code} = redirected_params(conn)

      assert redirected_to(conn) == pms_sprout_invitation_path(conn, :show, verification_code)

      conn = get(conn, pms_sprout_invitation_path(conn, :show, verification_code))
      assert html_response(conn, 200) =~ "Skicka denna länk"
    end
  end

  describe "delete sprout_invitation" do
    setup [:create_child_sprout_invitation]

    @tag :authenticated_user
    test "deletes chosen sprout_invitation", %{conn: conn, sprout_invitation: sprout_invitation} do
      conn = delete(conn, pms_sprout_invitation_path(conn, :delete, sprout_invitation))
      assert redirected_to(conn) == pms_sprout_path(conn, :show, sprout_invitation.sprout_id)

      assert_error_sent(404, fn ->
        get(conn, pms_sprout_invitation_path(conn, :show, sprout_invitation))
      end)
    end
  end

  defp create_child_sprout_invitation(%{user: %SproutTree.Accounts.User{} = user}) do
    sprout_invitation = fixture(:child_sprout_invitation, user)
    {:ok, sprout_invitation: sprout_invitation}
  end

  defp create_sprout(%{user: %SproutTree.Accounts.User{} = user}) do
    sprout = fixture(:sprout, user)

    {:ok, sprout: sprout}
  end
end
