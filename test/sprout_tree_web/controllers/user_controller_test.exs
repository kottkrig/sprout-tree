defmodule SproutTreeWeb.UserControllerTest do
  use SproutTreeWeb.ConnCase

  alias SproutTree.Accounts

  @create_attrs %{
    first_name: "some first_name",
    full_name: "some full_name",
    credential: %{email: "test@test.com", password: "1234"}
  }

  @update_attrs %{
    first_name: "some updated first_name",
    full_name: "some updated full_name"
  }

  @invalid_attrs %{first_name: nil, full_name: nil}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  # describe "index" do
  #   test "lists all users", %{conn: conn} do
  #     conn = get conn, user_path(conn, :index)
  #     assert html_response(conn, 200) =~ "Listing Users"
  #   end
  # end

  describe "new user" do
    test "renders form", %{conn: conn} do
      conn = get(conn, user_path(conn, :new))
      assert html_response(conn, 200) =~ "Skapa konto"
    end
  end

  describe "create user" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), user: @create_attrs)

      assert redirected_to(conn) == pms_sprout_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "Skapa konto"
    end
  end

  @tag :authenticated_user
  describe "edit user" do
    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = get(conn, pms_user_path(conn, :edit, user))
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "update user" do
    @tag :authenticated_user
    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put(conn, pms_user_path(conn, :update, user), user: @update_attrs)
      assert redirected_to(conn) == pms_user_path(conn, :show, user)

      conn = get(conn, pms_user_path(conn, :show, user))
      assert html_response(conn, 200) =~ "some updated first_name"
    end

    @tag :authenticated_user
    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, pms_user_path(conn, :update, user), user: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "delete user" do
    @tag :authenticated_user
    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, pms_user_path(conn, :delete, user))
      assert redirected_to(conn) == page_path(conn, :index)

      assert_error_sent(404, fn ->
        get(conn, pms_user_path(conn, :show, user))
      end)
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
